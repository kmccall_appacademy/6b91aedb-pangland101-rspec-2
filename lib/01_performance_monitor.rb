def measure(runs = 1)
  times = []

  runs.times do
    start = Time.now
    yield
    times << Time.now - start
  end

  return times[0] if runs == 1
  times.reduce(:+) / runs
end
