def reverser
  string = yield
  words = string.split(/\b/)
  result = ""

  words.each do |word|
    i = word.length - 1
    until i < 0
      result += word[i]
      i -= 1
    end
  end

  result
end

def adder(addon = 1)
  number = yield
  number + addon
end

def repeater(runs = 1)
  runs.times { yield }
end
